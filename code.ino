// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
#include "InternetButton.h"
#include "math.h"


InternetButton b = InternetButton();


// Global variables to store how many lights should be turned on
int choice = 1;
bool lightsChanged = false;


void setup() {

    // 1. Setup the Internet Button
    b.begin();

    
    Particle.function("choice", choiceSelected);
 
   
   
  
}




void loop(){
    
    // This loop just sits here and waits for the numLightsChanged variable to change to true
    // Once it changes to true, run the activateLEDS() function.
    if(lightsChanged == true){
        delay(10);
        activateLeds();
       
        lightsChanged = false;
    }
    
     if(b.buttonOn(2)){
        Particle.publish("answer","Animal",60,PUBLIC);
        delay(500);
        activateLeds();
        
    }
    
    if(b.buttonOn(4)){
        Particle.publish("answer","Fruit",60,PUBLIC);
        delay(500);
        activateLeds();
    }
}

// Turn on your LEDS
void activateLeds(){

    // 1. turn off all lights
    b.allLedsOff();
    
  
    
  
    
    if(choice == 1){
        // red for wrong choice
         b.allLedsOn(255, 0, 0);
         delay(1000);
         b.allLedsOff();
         
    }
    
    if(choice == 2){
        // green for right choice
        b.allLedsOn(64, 255, 0);
        delay(1000);
         b.allLedsOff();
       
    }
    
         
    }
    

    
    


/*
controlNumLights() is the local function that is executed when the API function "lights" is called.
It changes how many LEDs on the Button are illuminated.
*/
int choiceSelected(String command){
    //parse the string into an integer
    int cselected = atoi(command.c_str());

    
    // If no errors, then set the global variable to numLights
    choice = cselected;

    lightsChanged = true;

    // In embedded programming, "return 1" means that 
    // the function finished successfully
    return 1;
}